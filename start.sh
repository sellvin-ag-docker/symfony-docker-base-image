#!/bin/sh

# Force immediate synchronisation of the time and start the time-synchronization service.
# In order to be able to use ntpd in the container, it must be run with the SYS_TIME capability.
# In addition you may want to add the SYS_NICE capability, in order for ntpd to be able to modify its priority.
ntpd -s

# Apache server name change
sed -i "s/#ServerName www.example.com:80/ServerName pixiapps.test/" /etc/apache2/httpd.conf

# PHP Config
sed -i "s/\;\?\\s\?short_open_tag = .*/short_open_tag = On/" /etc/php7/php.ini
sed -i "s/\;\?\\s\?error_reporting = .*/error_reporting = E_ALL/" /etc/php7/php.ini
sed -i "s/\;\?\\s\?display_errors = .*/display_errors = On/" /etc/php7/php.ini
sed -i "s/\;\?\\s\?html_errors = .*/html_errors = On/" /etc/php7/php.ini
sed -i "s|;*date.timezone =.*|date.timezone = Europe/Berlin|i" /etc/php7/php.ini

# enable xdebug coverage for testing with phpunit (already installed)
echo "Enable XDebug..."
echo 'zend_extension=/usr/lib/php7/modules/xdebug.so' >> /etc/php7/php.ini;
echo 'xdebug.coverage_enable=On' >> /etc/php7/php.ini;
echo 'xdebug.remote_enable=1' >> /etc/php7/php.ini;
echo 'xdebug.remote_connect_back=1' >> /etc/php7/php.ini;
echo 'xdebug.remote_log=/tmp/xdebug.log' >> /etc/php7/php.ini;
echo 'xdebug.remote_autostart=true' >> /etc/php7/php.ini;


# Start (ensure apache2 PID not left behind first) to stop auto start crashes if didn't shut down properly
echo "Clearing any old processes..."
rm -f /run/apache2/apache2.pid
rm -f /run/apache2/httpd.pid

echo "Starting apache..."
httpd
